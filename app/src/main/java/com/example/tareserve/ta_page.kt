package com.example.tareserve

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ToggleButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tareserve.data.model.TeachingAssistant

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_PARAM3 = "param3"
private const val studentName = "Bob"
private var queueSize:Int = 0

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ta_page.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ta_page.newInstance] factory method to
 * create an instance of this fragment.
 */
class ta_page : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var param3: List<String>? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
            param3 = it.getStringArrayList(ARG_PARAM3)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_ta_page, container, false)
    }

    override fun onViewCreated(view: View, bundle: Bundle?) {
        super.onViewCreated(view, bundle)
        var name = this.arguments?.getString("name")
        var descr =  this.arguments?.getString("description")
        var queue = this.arguments?.getStringArrayList("queueAmount")
        view.findViewById<TextView>(R.id.TAPageName).text = name

        if(queue!!.isEmpty())
        {
            view.findViewById<TextView>(R.id.amountInQueue).text = "No one in queue!"
        }
        else
        {
            view.findViewById<TextView>(R.id.amountInQueue).text = "There are "+queue.size+" people in queue"
        }
        val enterQueue = view.findViewById<ToggleButton>(R.id.enqueueButton)
        checkToggle(enterQueue)
        enterQueue.setOnClickListener {
            if(enterQueue.isChecked)
            {
                queue.add(studentName)

                view.findViewById<TextView>(R.id.amountInQueue).text = "There are "+queue.size+" people in queue"

            }
            else
            {

                queue.remove(studentName)
                if(queue.size == 0)
                {
                    view.findViewById<TextView>(R.id.amountInQueue).text = "No one in queue!"
                }
                else
                {
                    view.findViewById<TextView>(R.id.amountInQueue).text = ""+queue.size+" people in queue"
                }
            }
            checkToggle(enterQueue)
        }
    }

    private fun checkToggle(button: ToggleButton)
    {
        if(button.isChecked)
        {
            button.text = "DEQUEUE"
        }
        else
        {
            button.text = "ENQUEUE"
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ta_page.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ta_page().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


}
