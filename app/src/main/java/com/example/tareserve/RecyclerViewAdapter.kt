package com.example.tareserve

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.tareserve.data.model.SchoolClass


class RecyclerViewAdapter(private val myDataset: ArrayList<SchoolClass>, private val activity: MainActivity) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    class ViewHolder(private val view: View, private val activity: MainActivity) : RecyclerView.ViewHolder(view){
        fun bindItems(classItem: SchoolClass) {
            val dept: TextView = itemView.findViewById(R.id.ClassName)
            val availability:TextView = itemView.findViewById(R.id.isFree)
            val lightColor:ImageView = itemView.findViewById(R.id.activeButton)

            if(classItem.TA.isEmpty())
            {
                availability.text = "No TA's Available"
                lightColor.setImageResource(R.drawable.red_button)
                lightColor.setBackgroundColor(16777215)
            }
            else
            {
                availability.text = ""+ classItem.TA.size + " TA's Available"
                lightColor.setImageResource(R.drawable.free_class)
                lightColor.setBackgroundColor(16777215)
            }
            dept.text = classItem.name


            itemView.setOnClickListener {
                view.findNavController().navigate(R.id.action_class_page_to_class_layout,
                    bundleOf("name" to classItem.name, "description" to classItem.description, "TA" to classItem.TA))
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecyclerViewAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.class_item, parent, false)
        return ViewHolder(v, activity)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(myDataset[position])
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}

