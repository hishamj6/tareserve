package com.example.tareserve.data.model

import androidx.navigation.NavType
import kotlinx.android.parcel.Parcelize

data class SchoolClass (var name: String, var description: String, var TA:ArrayList<TeachingAssistant>)
{
}