package com.example.tareserve

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tareserve.data.model.SchoolClass
import com.example.tareserve.data.model.TeachingAssistant
import com.google.firebase.database.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_PARAM3 = "param3"
private lateinit var recyclerView: RecyclerView
private lateinit var viewAdapter: RecyclerView.Adapter<*>
private val assistantItems = ArrayList<TeachingAssistant>()
private lateinit var database: DatabaseReference


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [class_layout.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [class_layout.newInstance] factory method to
 * create an instance of this fragment.
 */
class class_layout : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var param3: ArrayList<TeachingAssistant>? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
            param3 = it.getParcelableArrayList<TeachingAssistant>(ARG_PARAM3)
        }
    }

    //current dept
    var dept:String? = null
    var news:String? = null
    var TAList:ArrayList<TeachingAssistant>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        bundle: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_class_layout, container, false)


    }

    override fun onViewCreated(view: View, bundle: Bundle?) {
        super.onViewCreated(view, bundle)

        dept = this.arguments?.getString("name")
        news =  this.arguments?.getString("description")
        TAList = this.arguments?.getParcelableArrayList<TeachingAssistant>("TA")
        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerViewTA)
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        if(TAList != null) {
            viewAdapter = RecyclerViewAdapter(TAList!!, activity as MainActivity)
        }

        recyclerView.adapter = viewAdapter


    }


// TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment class_layout.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String, param3: String) =
            class_layout().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                    putString(ARG_PARAM3, param3)
                }
            }
    }

    class RecyclerViewAdapter(private val myDataset: ArrayList<TeachingAssistant>, private val activity: MainActivity) :
        RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

        class ViewHolder(private val view: View, private val activity: MainActivity) : RecyclerView.ViewHolder(view){
            fun bindItems(classItem: TeachingAssistant) {
                database = FirebaseDatabase.getInstance().getReferenceFromUrl(classItem.ref)

                val dept: TextView = itemView.findViewById(R.id.ClassName)
                val availability:TextView = itemView.findViewById(R.id.isFree)
                val lightColor: ImageView = itemView.findViewById(R.id.activeButton)

                dept.text = classItem.name

                var size = classItem.queueAmount.size

                val postListener = object : ValueEventListener {
                    override fun onCancelled(databaseError: DatabaseError) {

                    }

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        size = dataSnapshot.childrenCount.toInt()
                        if(size == 0)
                        {
                            availability.text = "  None\n in Queue"

                            lightColor.setImageResource(R.drawable.free_class)
                            lightColor.setBackgroundColor(16777215)
                        }
                        else
                        {
                            availability.text = " "+size + " in \nQueue"
                            lightColor.setImageResource(R.drawable.red_button)
                            lightColor.setBackgroundColor(16777215)
                        }
                    }
                }
                database.addValueEventListener(postListener)

                if(size == 0)
                {
                    availability.text = "  None\n in Queue"

                    lightColor.setImageResource(R.drawable.free_class)
                    lightColor.setBackgroundColor(16777215)
                }
                else
                {
                    availability.text = " "+ size + " in \nQueue"
                    lightColor.setImageResource(R.drawable.red_button)
                    lightColor.setBackgroundColor(16777215)
                }

                itemView.setOnClickListener {
                    view.findNavController().navigate(R.id.action_class_layout_to_ta_page,
                        bundleOf("name" to classItem.name, "description" to classItem.description, "queueAmount" to classItem.queueAmount, "reference" to classItem.ref)
                    )
                }
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup,
                                        viewType: Int): RecyclerViewAdapter.ViewHolder {
            val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.class_item, parent, false)
            return ViewHolder(v, activity)
        }
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindItems(myDataset[position])
        }

        // Return the size of your dataset (invoked by the layout manager)
        override fun getItemCount() = myDataset.size
    }
}
